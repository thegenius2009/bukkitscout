﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace BukkitScout
{
    class ConfigReader
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public ConfigReader(string configpath)
        {
            path = configpath;
        }
        
        public void WriteValue(string Key, string Value)
        {
            WritePrivateProfileString("Config", Key, Value, this.path);
        }

        public string ReadValue(string Key, string defaultvalue)
        {
            StringBuilder temp = new StringBuilder(65535);
            int i = GetPrivateProfileString("Config", Key, "", temp,
                                            65535, this.path);
            if (temp.ToString() == ""){return defaultvalue;}
            return temp.ToString();

        }
    }
}
