﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BukkitScout
{
    public class Program
    {
        static ConfigReader cfg;
        private static string cfg_path = Path.GetFullPath("bukkitscout.ini");
        static void Main(string[] args)
        {
            
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-c")
                {
                    i++;
                    cfg_path = Path.GetFullPath(args[i]);
                }
            }
            if (!File.Exists(cfg_path))
            {
                File.WriteAllText(cfg_path, "");
            }
            cfg = new ConfigReader(cfg_path);
        }
    }
}
